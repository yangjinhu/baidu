var that;
class Tab {
  constructor(id) {
    //   获取元素
    that = this;
    this.main = document.querySelector(id);
    this.add = this.main.querySelector(".tabadd");
    // li的父元素
    this.ul = this.main.querySelector(".firstnav ul:first-child");
    // section的父元素
    this.tabscon = this.main.querySelector(".tabscon");
    this.init();
  }
  init() {
    this.updateNode();
    // init初始化操作让相关的元素绑定事件
    this.add.onclick = that.addTab;
    for (let i = 0; i < this.lis.length; i++) {
      this.lis[i].index = i;
      this.lis[i].onclick = this.toggleTab;
      this.remove[i].onclick = this.removeTab;
      this.spans[i].ondblclick = this.editTab;
      this.sections[i].ondblclick = this.editTab;
      // this.divs.ondblclick = this.editTab;
    }
  }
  // 1.切换功能
  toggleTab() {
    that.clearClass();
    this.className = "liactive";
    that.sections[this.index].className = "conactive";
  }
  // 2.添加功能
  addTab() {
    that.clearClass();
    // a.创建li元素和section元素
    var li =
      '<li class="liactive"><span>新选项卡</span><span class="iconfont icon-guanbi"></span></li>';
    var section = ' <section class="conactive">测试a</section>';
    // b.把这两个元素追加到对应的父元素里面
    that.ul.insertAdjacentHTML("beforeend", li);
    that.tabscon.insertAdjacentHTML("beforeend", section);
    that.init();
  }
  // 3.删除功能
  removeTab(e) {
    e.stopPropagation();
    var index = this.parentNode.index;
    that.lis[index].remove();
    that.sections[index].remove();
    that.init();
    if (document.querySelector(".liactive")) return;
    index--;
    // 手动调用点击事件，不需要鼠标触发
    that.lis[index] && that.lis[index].click();
  }
  // 4.修改功能
  editTab() {
    var str = this.innerHTML;
    // 双击禁止选定文字
    // window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
    window.getSelection
      ? window.getSelection().removeAllRanges()
      : document.selection.empty();
    this.innerHTML = '<input type="text" />';
    var input = this.children[0];
    input.value = str;
    input.select();
    // 离开文本框就把文本框值赋给span
    input.onblur = function () {
      this.parentNode.innerHTML = this.value;
    };
    input.onkeyup = function (e) {
      if (e.keyCode === 13) {
        // 手动调用表单失去焦点事件
        this.blur();
      }
    };
  }
  clearClass() {
    this.updateNode();
    for (let i = 0; i < this.lis.length; i++) {
      this.lis[i].className = "";
      this.sections[i].className = "";
    }
  }
  // 获取所有的li和section
  updateNode() {
    this.lis = this.main.querySelectorAll("li");
    this.sections = this.main.querySelectorAll("section");
    // this.divs = this.main.querySelectorAll("section div");
    this.remove = this.main.querySelectorAll(".icon-guanbi");
    this.spans = this.main.querySelectorAll(".firstnav li span:first-child");
  }
}
new Tab("#tab");
