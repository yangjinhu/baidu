window.onload = function(){
	var tag_heart = document.getElementsByClassName("heart");
	for(let i =0 ; i <tag_heart.length; i++)//爱心点赞事件绑定
	{
		tag_heart[i].onclick = function(e){
			
			var rel = e.target.getAttribute("rel");
			var id = e.target.getAttribute("id");
			
			if (rel === 'like') {
				var test =  document.getElementById(id);
				test.setAttribute('class','heart heartAnimation');
				test.setAttribute('rel','unlike');
				return
			}
			else {
				var test =  document.getElementById(id);
				test.setAttribute('class','heart');
				test.setAttribute('rel','like');
			    
			}
		}
	}
	//留言板事件绑定
	
	document.getElementById('comment-submit-id').addEventListener('click', function(){
		var input = document.getElementsByClassName('comment-input')
		var user = document.getElementsByClassName('comment-user')
		var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]")
		var text_input = input[0].value
		var text_user = user[0].value
		if(text_user.length<3){
			alert('昵称少于三个字符！')
			document.getElementsByClassName('comment-user')[0].value=""
			return
		}else if(text_user.length>6){
			alert('昵称多于六个字符！')
			document.getElementsByClassName('comment-user')[0].value=""
			return
		}else {
			if(checkName(text_input)){
				document.getElementsByClassName('comment-input')[0].value=""
				
				return
			}else if(checkName(text_user)){
				document.getElementsByClassName('comment-user')[0].value=""
				return
			}
		}
		if(text_input.match('<script>')||text_input.match('<body>')||text_input.match('<style>')||text_input.match('<div>')||text_input.match('<p>')||text_input.match('<br>')||text_input.match('<img>')||text_input.match('<html>')){
			alert('您输入的语句有潜在Xss危险，请重新输入')
			document.getElementsByClassName('comment-input')[0].value=""
			return
		}//xss验证
		var html = document.getElementById("message-id").innerHTML
		
		document.getElementById('message-id').innerHTML =html+ `<div class="message-text">
						<div class="message-head">
							<div class="message-name">`
								+text_user+
							`</div>
						</div>
						<div class="message-context">
							<p>`+text_input +`</p>
						</div>
					</div>`
	});
	function checkName(newName){
			var regEn = /[`@#$%^&*()_+<>?:"{}\/'[\]]/im,
			    regCn = /[·#￥（——）“”‘、，| 、【】[\]]/im;
			if(regEn.test(newName) || regCn.test(newName)) {
			   alert('您输入了非法字符，请重新输入');
			    return true;
			}
			return false;
	}//利用正则表达式进行非法字符验证
}

// $("#comment-submit").on("click", function () {
//     // var wrapper = $('.comment-wrapper');
//     var wrapper = document.getElementById('.comment-wrapper')
//     console.log(wrapper);
//     // 获取元素
//     // var input = $('.comment-input');;
//     // var user = $('.comment-user');;
//     // var submit = $('.comment-submit');;
//     var input = document.getElementById('.comment-input');
//     var user = document.getElementById('.comment-user');
//     var submin = document.getElementById('.comment-submit');
    
//   });