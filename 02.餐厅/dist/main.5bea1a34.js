// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

(function(modules, cache, entry, globalName) {
  /* eslint-disable no-undef */
  var globalObject =
    typeof globalThis !== 'undefined'
      ? globalThis
      : typeof self !== 'undefined'
      ? self
      : typeof window !== 'undefined'
      ? window
      : typeof global !== 'undefined'
      ? global
      : {};
  /* eslint-enable no-undef */

  // Save the require from previous bundle to this closure if any
  var previousRequire =
    typeof globalObject.parcelRequire === 'function' &&
    globalObject.parcelRequire;
  // Do not use `require` to prevent Webpack from trying to bundle this call
  var nodeRequire =
    typeof module !== 'undefined' &&
    typeof module.require === 'function' &&
    module.require.bind(module);

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire =
          typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error("Cannot find module '" + name + "'");
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = (cache[name] = new newRequire.Module(name));

      modules[name][0].call(
        module.exports,
        localRequire,
        module,
        module.exports,
        this
      );
    }

    return cache[name].exports;

    function localRequire(x) {
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x) {
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function(id, exports) {
    modules[id] = [
      function(require, module) {
        module.exports = exports;
      },
      {},
    ];
  };

  globalObject.parcelRequire = newRequire;

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === 'object' && typeof module !== 'undefined') {
      module.exports = mainExports;

      // RequireJS
    } else if (typeof define === 'function' && define.amd) {
      define(function() {
        return mainExports;
      });

      // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }
})({"283ca64e65832371ed077ca9269a5eb3":[function(require,module,exports) {
var global = arguments[3];
var HMR_HOST = null;
var HMR_PORT = 1234;
var HMR_ENV_HASH = "d751713988987e9331980363e24189ce";
module.bundle.HMR_BUNDLE_ID = "5bea1a3498d97696fecc91f1d17e7d8f";
/* global HMR_HOST, HMR_PORT, HMR_ENV_HASH */

var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept, acceptedAssets; // eslint-disable-next-line no-redeclare

var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = HMR_HOST || (location.protocol.indexOf('http') === 0 ? location.hostname : 'localhost');
  var port = HMR_PORT || location.port;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + (port ? ':' + port : '') + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    acceptedAssets = {};
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      // Remove error overlay if there is one
      removeErrorOverlay();
      let assets = data.assets.filter(asset => asset.envHash === HMR_ENV_HASH); // Handle HMR Update

      var handled = false;
      assets.forEach(asset => {
        var didAccept = asset.type === 'css' || hmrAcceptCheck(global.parcelRequire, asset.id);

        if (didAccept) {
          handled = true;
        }
      });

      if (handled) {
        console.clear();
        assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });

        for (var i = 0; i < assetsToAccept.length; i++) {
          var id = assetsToAccept[i][1];

          if (!acceptedAssets[id]) {
            hmrAcceptRun(assetsToAccept[i][0], id);
          }
        }
      } else {
        window.location.reload();
      }
    }

    if (data.type === 'error') {
      // Log parcel errors to console
      for (let ansiDiagnostic of data.diagnostics.ansi) {
        let stack = ansiDiagnostic.codeframe ? ansiDiagnostic.codeframe : ansiDiagnostic.stack;
        console.error('🚨 [parcel]: ' + ansiDiagnostic.message + '\n' + stack + '\n\n' + ansiDiagnostic.hints.join('\n'));
      } // Render the fancy html overlay


      removeErrorOverlay();
      var overlay = createErrorOverlay(data.diagnostics.html);
      document.body.appendChild(overlay);
    }
  };

  ws.onerror = function (e) {
    console.error(e.message);
  };

  ws.onclose = function (e) {
    console.warn('[parcel] 🚨 Connection to the HMR server was lost');
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
    console.log('[parcel] ✨ Error resolved');
  }
}

function createErrorOverlay(diagnostics) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID;
  let errorHTML = '<div style="background: black; opacity: 0.85; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; font-family: Menlo, Consolas, monospace; z-index: 9999;">';

  for (let diagnostic of diagnostics) {
    let stack = diagnostic.codeframe ? diagnostic.codeframe : diagnostic.stack;
    errorHTML += `
      <div>
        <div style="font-size: 18px; font-weight: bold; margin-top: 20px;">
          🚨 ${diagnostic.message}
        </div>
        <pre>
          ${stack}
        </pre>
        <div>
          ${diagnostic.hints.map(hint => '<div>' + hint + '</div>').join('')}
        </div>
      </div>
    `;
  }

  errorHTML += '</div>';
  overlay.innerHTML = errorHTML;
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push([bundle, k]);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function updateLink(link) {
  var newLink = link.cloneNode();

  newLink.onload = function () {
    if (link.parentNode !== null) {
      link.parentNode.removeChild(link);
    }
  };

  newLink.setAttribute('href', link.getAttribute('href').split('?')[0] + '?' + Date.now());
  link.parentNode.insertBefore(newLink, link.nextSibling);
}

var cssTimeout = null;

function reloadCSS() {
  if (cssTimeout) {
    return;
  }

  cssTimeout = setTimeout(function () {
    var links = document.querySelectorAll('link[rel="stylesheet"]');

    for (var i = 0; i < links.length; i++) {
      var absolute = /^https?:\/\//i.test(links[i].getAttribute('href'));

      if (!absolute) {
        updateLink(links[i]);
      }
    }

    cssTimeout = null;
  }, 50);
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    if (asset.type === 'css') {
      reloadCSS();
    } else {
      var fn = new Function('require', 'module', 'exports', asset.output);
      modules[asset.id] = [fn, asset.depsByBundle[bundle.HMR_BUNDLE_ID]];
    }
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (v) {
    return hmrAcceptCheck(v[0], v[1]);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached && cached.hot) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      var assetsToAlsoAccept = cb(function () {
        return getParents(global.parcelRequire, id);
      });

      if (assetsToAlsoAccept && assetsToAccept.length) {
        assetsToAccept.push.apply(assetsToAccept, assetsToAlsoAccept);
      }
    });
  }

  acceptedAssets[id] = true;
}
},{}],"aa97c85f9dce1163dc0f1b0575ce0e5f":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.settotal = settotal;

var _adminis = _interopRequireDefault(require("./js/adminis"));

require("./js/header");

var _guest = require("./js/guest");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var guest_dish = "你好head"; // admi();
// guest();
// 获取客人名字和所点菜总钱数

var total_money = document.getElementById("total-money");
var total = 0;

var statusInspect = function (inputs) {
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener("change", function () {
      if (inputs[i].checked) {
        for (let j = 0; j < inputs.length; j++) {
          if (j == i) {
            continue;
          }

          inputs[j].disabled = true;
        }
      } else {
        for (let j = 0; j < inputs.length; j++) {
          inputs[j].disabled = false;
        }
      }
    });
  }
}; // 实时获取菜单选中状态


var checkboxs_status = document.getElementsByName("status");

for (let i = 0; i < checkboxs_status.length; i++) {
  checkboxs_status[i].addEventListener("change", function () {
    if (checkboxs_status[i].checked) {
      total += Number(checkboxs_status[i].parentNode.lastElementChild.innerHTML); // console.log(checkboxs_status[i].parentNode.lastElementChild.innerHTML);
    } else {
      total -= Number(checkboxs_status[i].parentNode.lastElementChild.innerHTML); // console.log(checkboxs_status[i].parentNode.lastElementChild.innerHTML);

      total_money.innerHTML = total;
    }
  });
}

setInterval(function () {
  total_money.innerHTML = total;
}, 1000); // 设置五选一以及必须点一个主菜

var main_status = document.querySelectorAll(".main-dish input");
statusInspect(main_status); // 设置凉菜以及饮品二选一

var cold_status = document.querySelectorAll(".cold_dish input");
statusInspect(cold_status);
var drinks = document.querySelectorAll(".drink input");
statusInspect(drinks);
console.log(guest_dish);

function settotal() {
  total = 0;
}
},{"./js/adminis":"faecc4e7c5bd46b5cc435cb20064877e","./js/header":"1be67bb5218d207fca3cafff57ad0fc4","./js/guest":"968ce1e59617434a5b9cf9c38a92823f"}],"faecc4e7c5bd46b5cc435cb20064877e":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _header = require("./header");

// import {w,h,money,chief} from "./bianliang";
// 定义初始厨师数量为1
var chief = 1;
var that;

class Administrator {
  constructor() {
    that = this; // 获取父容器

    this.chiefs_wrapper = document.querySelector(".chiefs-wrapper"); // 获取厨师的最后一个

    this.chief_wrapper = document.querySelector(".chiefs-wrapper .chief-wrapper"); // 获取招聘厨师容器

    this.chief_add_wrapper = document.querySelector(".chief_add-wrapper"); //this.chief_add_wrapper  =document.getElementsByClassName('chief_add-wrapper')[0];
    //   获取招聘厨师

    this.chief_add = document.querySelector(".chief_add");
    this.zhezhao3 = document.getElementById("zhezhao3");
    this.confirm_ok = document.querySelector(".confirm-ok");
    this.confirm_no = document.querySelector(".confirm-no");
    this.recruit_title = document.querySelector(".recruit-title");
    this.zhezhao4 = document.querySelector("#zhezhao4");
    this.fire_btns = document.querySelector(".fire-btns");
    this.init();
  }

  init() {
    this.chief_add.onclick = that.alertrecruit; // 不要放在aleri里面，那会添加多个监听事件

    that.confirm_ok.addEventListener("click", function (ev) {
      that.recruit();
      ev.stopPropagation();
      that.zhezhao3.style.display = "none";
    });
    that.confirm_no.addEventListener("click", function () {
      that.zhezhao3.style.display = "none";
    }); // 解雇失败提示

    that.zhezhao4.children[0].children[3].addEventListener("click", function () {
      this.style.display = "none";
    }); // 解雇成功提示

    that.zhezhao4.children[0].children[4].addEventListener("click", function () {
      this.style.display = "none";
    });
    that.fire_btns.children[0].addEventListener("click", function () {
      if (_header.money < 140) {
        that.zhezhao4.children[0].children[3].style.display = "block";
      } else {
        that.zhezhao4.children[0].children[4].style.display = "block";
      }
    });
    that.fire_btns.children[1].addEventListener("click", function () {
      that.zhezhao4.style.display = "none";
    });
  }

  alertrecruit() {
    that.zhezhao3.style.display = "block";
  }

  recruit() {
    // debugger;
    chief++;

    if (chief >= 6) {
      that.chief_add_wrapper.style.display = "none";
    }

    let chief_wrapper = '<div class="chief-wrapper"><div class="chief free"><span class="chief-dish"></span><div class="finish"></div><span class="fire">×</span></div></div>';
    that.chief_wrapper.insertAdjacentHTML("afterend", chief_wrapper);
    var fire = document.querySelector(".fire");
    fire.addEventListener("click", function () {
      that.zhezhao4.style.display = "block"; // chief--;
      // this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
    });
    that.recruit_title.style.display = "block";
    that.recruit_title.children[0].innerHTML = chief;
    that.recruit_title.addEventListener("click", function (ev) {
      this.style.display = "none"; // ev.stopPropagation();
    });
  }

} // new Administrator()


var admi = new Administrator();
var _default = admi; // module.exports = {
//   admi
// };
// export {admi};
// export class ClassName {Administrator}

exports.default = _default;
},{"./header":"1be67bb5218d207fca3cafff57ad0fc4"}],"1be67bb5218d207fca3cafff57ad0fc4":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addmoney = addmoney;
exports.submoney = submoney;
exports.money = void 0;
// import {w,h,money,chief} from "./bianliang";
// 分别定义w周、d天以及金钱元素变量
var w = 1,
    h = 1,
    money = 100; // 初始化一个日期对象

exports.money = money;
var initdate = new Date(); // 如果通过设置世界时的方式要考虑天、小时、分钟等因素，较为繁琐
// date.setUTCHours(0);
// date.setUTCMinutes(0,0,0)

let date = null;
var interval = setInterval(function () {
  // console.log(w, h);
  date = new Date(); // console.log(date.getTime());
  //   设置每天为240s

  if ((date.getTime() - initdate.getTime()) / 240000 > 1) {
    h++;

    if (h > 7) {
      w++;
      h = 0;
    }

    initdate = date;
  }
}, 1000); // 获取w元素

var date_w = document.querySelector(".date-w");
date_w.innerHTML = "w" + w; // 获取h元素

var date_h = document.querySelector(".date-h");
date_h.innerHTML = "h" + h; // 获取money元素

var res_money = document.querySelector(".res-money");
setInterval(function () {
  res_money.innerHTML = money;
}, 1000);

function addmoney(n) {
  exports.money = money = money + n;
}

function submoney(n) {
  exports.money = money = money - n;
}
},{}],"968ce1e59617434a5b9cf9c38a92823f":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.names = exports.guest = void 0;

var _header = require("./header");

var _main = require("../main");

// import { guest_dish } from "../main";
// 设置点击后遮罩层消失
let guest_dish = "酥炸Echarts";
let main_status = document.querySelectorAll(".main-dish input");
let checkboxs_status = document.getElementsByName("status");
let zhezhao2 = document.getElementById("zhezhao2");
let btn_ok = document.getElementById("btn_ok");
btn_ok.addEventListener("click", function () {
  let i = 0;

  for (i; i < main_status.length;) {
    if (!main_status[i].checked) {
      i++;
    } else {
      break;
    }
  }

  if (i >= main_status.length) {
    alert("必须要点一个主菜哦");
  } else {
    for (let i = 0; i < checkboxs_status.length; i++) {
      if (checkboxs_status[i].checked) {
        guest_dish = checkboxs_status[i].parentNode.children[1].innerHTML; // console.log(guest_dish);

        (0, _header.submoney)(9);
      }

      i = 0;

      while (checkboxs_status[i]) {
        checkboxs_status[i].checked = false;
        checkboxs_status[i].disabled = false;
        i++;
      }
    } // 将点菜总钱数置空


    (0, _main.settotal)();
    zhezhao2.style.display = "none";
  }
});
var that;
var names = ["one", "two", "three", "four"];
exports.names = names;

class Guest {
  constructor() {
    that = this;
    this.guests = document.getElementsByClassName("guest");
    this.zhezhao2 = document.getElementById("zhezhao2");
    this.user_name = document.getElementById("user-name");
    this.seats = document.getElementsByClassName("seat no");
    this.guest_name = document.getElementsByClassName("guest-name");
    this.chiefs = document.querySelectorAll(".free");
    that.seats[0].children[2].addEventListener("click", function () {
      // if (this.style.display == "block") {
      //   this.style.block = "none";
      // }
      that.seats[0].children[2].style.display = "none";
      that.chiefs[0].children[1].style.display = "none";
      that.chiefs[0].classList.remove("cooking");
      that.chiefs[0].classList.add("free");
      that.chiefs[0].children[0].style.display = "none";
      setTimeout(function () {
        that.seats[0].children[3].style.display = "block";
      }, 3000);
    });
    that.seats[0].children[3].addEventListener("click", function () {
      debugger;
      (0, _header.addmoney)(18);
      this.parentNode.style.backgroundImage = null;
      this.parentNode.children[1].style.display = "none";
      this.style.display = "none";
      this.parentNode.children[0].children[1].innerHTML = "已完成用餐,收获$18";
      this.parentNode.children[0].style.display = "block";
    });
    this.init();
  }

  init() {
    for (let i = 0; i < this.guests.length; i++) {
      this.guests[i].name = names[i];
      this.guests[i].addEventListener("click", that.takeseat);
      this.guests[i].addEventListener("click", that.alertmenu);
    }
  }

  alertmenu() {
    that.zhezhao2.style.display = "block";
    that.user_name.innerHTML = this.name;
    that.guest_name[0].innerHTML = this.name;
    that.guest_name[0].parentNode.children[1].innerHTML = "完成点餐，等候用餐，疯狂点击厨师头像可以加速做菜";
  }

  titleclick() {
    this.style.display = "none";
  } // 厨师开始做菜


  cooking() {
    that.chiefs[0].children[0].style.display = "block";
    that.chiefs[0].children[0].innerHTML = guest_dish;
    that.chiefs[0].classList.remove("free");
    that.chiefs[0].classList.add("cooking");
    var start = 0,
        end = 100,
        step = 8;
    that.chiefs[0].addEventListener("click", function () {
      step = 15;
    });
    var percentage = setInterval(function () {
      // console.log(start);
      start += step;
      end = 100 - start;
      that.chiefs[0].children[0].style.background = "linear-gradient(to right, #006dd9 " + start + "%, #2693ff " + end + "%)"; // var seats_has = document.getElementsByClassName("has");
      // console.log(seats_has);
      // that.seats[0].children[1].style.backgroundColor =null;

      that.seats[0].children[1].style.background = "linear-gradient(to right, #b20000 " + start + "%, #ff2626 " + end + "%)";
    }, 1000);
    setInterval(function () {
      if (start >= 100) {
        that.chiefs[0].children[1].style.display = "block";
        that.seats[0].children[2].style.display = "block";
        start = 0;
        end = 100;
        clearInterval(percentage);
      }
    }, 1000);
  } // 设置做菜进度


  takeseat() {
    var style = window.getComputedStyle(this); // console.log(style.backgroundImage);
    // that.seats[0].classList.remove("no");
    // that.seats[0].classList.add("has");

    that.seats[0].style.backgroundImage = style.backgroundImage;
    that.seats[0].style.backgroundSize = style.backgroundSize;
    that.seats[0].children[0].style.display = "block";
    that.seats[0].children[0].addEventListener("click", that.titleclick);
    that.seats[0].children[1].style.display = "block";
    that.seats[0].children[1].innerHTML = guest_dish; // 客人入座消失

    this.parentNode.style.display = "none";
    that.alertmenu();
    that.cooking();
  }

} // new Guest();


var guest = new Guest();
exports.guest = guest;
},{"./header":"1be67bb5218d207fca3cafff57ad0fc4","../main":"aa97c85f9dce1163dc0f1b0575ce0e5f"}]},{},["283ca64e65832371ed077ca9269a5eb3","aa97c85f9dce1163dc0f1b0575ce0e5f"], null)

//# sourceMappingURL=main.5bea1a34.js.map
