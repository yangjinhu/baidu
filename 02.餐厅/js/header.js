// import {w,h,money,chief} from "./bianliang";
// 分别定义w周、d天以及金钱元素变量
var w = 1,
  h = 1,
  money = 100;

// 初始化一个日期对象
var initdate = new Date();

// 如果通过设置世界时的方式要考虑天、小时、分钟等因素，较为繁琐
// date.setUTCHours(0);
// date.setUTCMinutes(0,0,0)

let date = null;
var interval = setInterval(function () {
  // console.log(w, h);
  date = new Date();
  // console.log(date.getTime());
  //   设置每天为240s
  if ((date.getTime() - initdate.getTime()) / 240000 > 1) {
    h++;
    if (h > 7) {
      w++;
      h = 0;
    }
    initdate = date;
  }
}, 1000);

// 获取w元素
var date_w = document.querySelector(".date-w");
date_w.innerHTML = "w" + w;

// 获取h元素
var date_h = document.querySelector(".date-h");
date_h.innerHTML = "h" + h;

// 获取money元素
var res_money = document.querySelector(".res-money");
setInterval(function () {
  res_money.innerHTML = money;
}, 1000);

export function addmoney(n) {
  money += n;
}
export function submoney(n){
  money -= n;
}
export { money };
