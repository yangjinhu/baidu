// import {w,h,money,chief} from "./bianliang";
// 定义初始厨师数量为1
import { submoney } from "./header";
import { money } from "./header";

var chief = 1;

var that;
class Administrator {
  constructor() {
    that = this;
    // 获取父容器
    this.chiefs_wrapper = document.querySelector(".chiefs-wrapper");
    // 获取厨师的最后一个
    this.chief_wrapper = document.querySelector(
      ".chiefs-wrapper .chief-wrapper"
    );
    // 获取招聘厨师容器
    this.chief_add_wrapper = document.querySelector(".chief_add-wrapper");
    //this.chief_add_wrapper  =document.getElementsByClassName('chief_add-wrapper')[0];
    //   获取招聘厨师
    this.chief_add = document.querySelector(".chief_add");
    this.zhezhao3 = document.getElementById("zhezhao3");
    this.confirm_ok = document.querySelector(".confirm-ok");
    this.confirm_no = document.querySelector(".confirm-no");
    this.recruit_title = document.querySelector(".recruit-title");
    this.zhezhao4 = document.querySelector("#zhezhao4");
    this.fire_btns = document.querySelector(".fire-btns");
    this.init();
  }
  init() {
    this.chief_add.onclick = that.alertrecruit;
    // 不要放在aleri里面，那会添加多个监听事件
    that.confirm_ok.addEventListener("click", function (ev) {
      that.recruit();
      ev.stopPropagation();
      that.zhezhao3.style.display = "none";
    });
    that.confirm_no.addEventListener("click", function () {
      that.zhezhao3.style.display = "none";
    });
    // 解雇失败提示
    that.zhezhao4.children[0].children[3].addEventListener(
      "click",
      function () {
        this.style.display = "none";
      }
    );
    // 解雇成功提示
    that.zhezhao4.children[0].children[4].addEventListener(
      "click",
      function () {
        this.style.display = "none";
      }
    );
    that.fire_btns.children[0].addEventListener("click", function () {
      if (money < 140) {
        that.zhezhao4.children[0].children[3].style.display = "block";
      } else {
        that.zhezhao4.children[0].children[4].style.display = "block";
      }
    });
    that.fire_btns.children[1].addEventListener("click", function () {
      that.zhezhao4.style.display = "none";
    });
  }
  alertrecruit() {
    that.zhezhao3.style.display = "block";
  }
  recruit() {
    // debugger;
    chief++;
    if (chief >= 6) {
      that.chief_add_wrapper.style.display = "none";
    }
    let chief_wrapper =
      '<div class="chief-wrapper"><div class="chief free"><span class="chief-dish"></span><div class="finish"></div><span class="fire">×</span></div></div>';
    that.chief_wrapper.insertAdjacentHTML("afterend", chief_wrapper);
    var fire = document.querySelector(".fire");
    fire.addEventListener("click", function () {
      that.zhezhao4.style.display = "block";

      // chief--;
      // this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);
    });
    that.recruit_title.style.display = "block";
    that.recruit_title.children[0].innerHTML = chief;
    that.recruit_title.addEventListener("click", function (ev) {
      this.style.display = "none";
      // ev.stopPropagation();
    });
  }
}

// new Administrator()
var admi = new Administrator();
export default admi;
// module.exports = {
//   admi
// };

// export {admi};
// export class ClassName {Administrator}
