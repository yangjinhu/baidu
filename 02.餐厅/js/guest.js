// import { guest_dish } from "../main";
import { addmoney, submoney } from "./header";
import { settotal } from "../main";

// 设置点击后遮罩层消失
let guest_dish = "酥炸Echarts";

let main_status = document.querySelectorAll(".main-dish input");
let checkboxs_status = document.getElementsByName("status");
let zhezhao2 = document.getElementById("zhezhao2");
let btn_ok = document.getElementById("btn_ok");
btn_ok.addEventListener("click", function () {
  let i = 0;
  for (i; i < main_status.length; ) {
    if (!main_status[i].checked) {
      i++;
    } else {
      break;
    }
  }
  if (i >= main_status.length) {
    alert("必须要点一个主菜哦");
  } else {
    for (let j = 0; j < checkboxs_status.length; j++) {
      if (checkboxs_status[j].checked) {
        // guest_dish = checkboxs_status[i].parentNode.children[1].innerHTML;
        // console.log(guest_dish);
        submoney(9);
      }
    }
    i = 0;
    while (checkboxs_status[i]) {
      checkboxs_status[i].checked = false;
      checkboxs_status[i].disabled = false;
      i++;
    }
    // 将点菜总钱数置空
    settotal();
    zhezhao2.style.display = "none";
  }
});


var that;
var names = ["one", "two", "three", "four"];
class Guest {
  constructor() {
    that = this;
    this.guests = document.getElementsByClassName("guest");
    this.zhezhao2 = document.getElementById("zhezhao2");
    this.user_name = document.getElementById("user-name");
    this.seats = document.getElementsByClassName("seat no");
    this.guest_name = document.getElementsByClassName("guest-name");
    this.chiefs = document.querySelectorAll(".free");
    that.seats[0].children[2].addEventListener("click", function () {
      // if (this.style.display == "block") {
      //   this.style.block = "none";
      // }
      that.seats[0].children[2].style.display = "none";
      that.chiefs[0].children[1].style.display = "none";
      that.chiefs[0].classList.remove("cooking");
      that.chiefs[0].classList.add("free");
      that.chiefs[0].children[0].style.display = "none";

      setTimeout(function () {
        that.seats[0].children[3].style.display = "block";
      }, 3000);
    });
    that.seats[0].children[3].addEventListener("click", function () {
      addmoney(18);
      this.parentNode.style.backgroundImage = null;
      this.parentNode.children[1].style.display = "none";
      this.style.display = "none";
      this.parentNode.children[0].children[1].innerHTML = "已完成用餐,收获$18";
      this.parentNode.children[0].style.display = "block";
    });
    this.init();
  }
  init() {
    for (let i = 0; i < this.guests.length; i++) {
      this.guests[i].name = names[i];
      this.guests[i].addEventListener("click", that.takeseat);
      this.guests[i].addEventListener("click", that.alertmenu);
    }
  }

  alertmenu() {
    that.zhezhao2.style.display = "block";
    that.user_name.innerHTML = this.name;
    that.guest_name[0].innerHTML = this.name;
    that.guest_name[0].parentNode.children[1].innerHTML =
      "完成点餐，等候用餐，疯狂点击厨师头像可以加速做菜";
  }
  titleclick() {
    this.style.display = "none";
  }
  // 厨师开始做菜
  cooking() {
    that.chiefs[0].children[0].style.display = "block";
    that.chiefs[0].children[0].innerHTML = guest_dish;

    that.chiefs[0].classList.remove("free");
    that.chiefs[0].classList.add("cooking");

    var start = 0,
      end = 100,
      step = 8;
    that.chiefs[0].addEventListener("click", function () {
      step = 15;
    });

    var percentage = setInterval(function () {
      // console.log(start);
      start += step;
      end = 100 - start;
      that.chiefs[0].children[0].style.background =
        "linear-gradient(to right, #006dd9 " +
        start +
        "%, #2693ff " +
        end +
        "%)";
      // var seats_has = document.getElementsByClassName("has");
      // console.log(seats_has);
      // that.seats[0].children[1].style.backgroundColor =null;
      that.seats[0].children[1].style.background =
        "linear-gradient(to right, #b20000 " +
        start +
        "%, #ff2626 " +
        end +
        "%)";
    }, 1000);
    setInterval(function () {
      if (start >= 100) {
        that.chiefs[0].children[1].style.display = "block";
        that.seats[0].children[2].style.display = "block";
        start = 0;
        end = 100;
        clearInterval(percentage);
      }
    }, 1000);
  }
  // 设置做菜进度

  takeseat() {
    var style = window.getComputedStyle(this);
    // console.log(style.backgroundImage);
    // that.seats[0].classList.remove("no");
    // that.seats[0].classList.add("has");
    that.seats[0].style.backgroundImage = style.backgroundImage;
    that.seats[0].style.backgroundSize = style.backgroundSize;
    that.seats[0].children[0].style.display = "block";
    that.seats[0].children[0].addEventListener("click", that.titleclick);
    that.seats[0].children[1].style.display = "block";
    that.seats[0].children[1].innerHTML = guest_dish;
    // 客人入座消失
    this.parentNode.style.display = "none";
    that.alertmenu();
    that.cooking();
  }
}

// new Guest();
var guest = new Guest();
export { guest, names };
