import admi from "./js/adminis";
import "./js/header";
import { guest, names } from "./js/guest";

var guest_dish = "你好head";
// admi();
// guest();

// 获取客人名字和所点菜总钱数
var total_money = document.getElementById("total-money");
var total = 0;

var statusInspect = function (inputs) {
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener("change", function () {
      if (inputs[i].checked) {
        for (let j = 0; j < inputs.length; j++) {
          if (j == i) {
            continue;
          }
          inputs[j].disabled = true;
        }
      } else {
        for (let j = 0; j < inputs.length; j++) {
          inputs[j].disabled = false;
        }
      }
    });
  }
};

// 实时获取菜单选中状态
var checkboxs_status = document.getElementsByName("status");
for (let i = 0; i < checkboxs_status.length; i++) {
  checkboxs_status[i].addEventListener("change", function () {
    if (checkboxs_status[i].checked) {
      total += Number(
        checkboxs_status[i].parentNode.lastElementChild.innerHTML
      );
      // console.log(checkboxs_status[i].parentNode.lastElementChild.innerHTML);
     
    } else {
      total -= Number(
        checkboxs_status[i].parentNode.lastElementChild.innerHTML
      );
      // console.log(checkboxs_status[i].parentNode.lastElementChild.innerHTML);
      total_money.innerHTML = total;
    }
  });
}
setInterval(function(){
  total_money.innerHTML = total;
},1000)
// 设置五选一以及必须点一个主菜
var main_status = document.querySelectorAll(".main-dish input");
statusInspect(main_status);

// 设置凉菜以及饮品二选一
var cold_status = document.querySelectorAll(".cold_dish input");
statusInspect(cold_status);
var drinks = document.querySelectorAll(".drink input");
statusInspect(drinks);
console.log(guest_dish);

export function settotal(){
  total = 0;
}
